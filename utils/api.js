import { promises as fs } from 'fs'
import path from 'path'
import * as matter from 'gray-matter'

const getModelFolder = async (modelFolder) => {
	const modelFolderPath = path.join(
		process.cwd(),
		'content',
		modelFolder
	)

	const modelItemFolders = await fs.readdir(modelFolderPath)

	const modelItems = modelItemFolders.map(modelItemFolder => {
		return getModelItem(modelFolder, modelItemFolder)
	})

	return Promise.all(modelItems)
}

const getModelItem = async (modelFolder, modelItemFolderSlug) => {
	const modelItemFolderPath = path.join(
		process.cwd(),
		'content',
		modelFolder,
		modelItemFolderSlug
	)
	const modelFilePath = path.join(modelItemFolderPath, 'index.md')

	let modelFileContent
	try {
		modelFileContent = await fs.readFile(modelFilePath, 'utf8')
	} catch (error) {
		console.error(`Missing file:`, modelFilePath)
	}

	let modelArtifacts
	try {
		modelArtifacts = await getModelItemArtifacts(modelFolder, modelItemFolderSlug)
	} catch (error) {
		console.error(`Error getting artifacts files in folder:`, modelItemFolderPath)
	}

	let model,
			frontmatter;
	if (modelFileContent) {
		frontmatter = matter(modelFileContent)
		model = {
			slug: modelItemFolderSlug,
			content: frontmatter.content,
			frontmatter: frontmatter.data,
			artifacts: modelArtifacts
		}
	}
	return model
}

const getModelItemArtifacts = async (modelFolder, modelItemFolderSlug) => {
	const modelItemFolderPath = path.join(
		process.cwd(),
		'content',
		modelFolder,
		modelItemFolderSlug
	)

	/* filter out elements with extension '.md' */
	const modelFilenames = await fs.readdir(modelItemFolderPath)
	const modelArtifacts = modelFilenames.filter(artifactFilename => {
		return ['md'].indexOf(artifactFilename.split('.')[1]) < 0
	}).map((artifactFilename) => {
		return getModelItemArtifact(
			modelFolder,
			modelItemFolderSlug,
			artifactFilename
		)
	})
	return await Promise.all(modelArtifacts)
}

const getModelItemArtifact = async (modelFolder, modelItemFolderSlug, artifactFilename) => {
	const artifactFilePath = path.join(
		process.cwd(),
		'content',
		modelFolder,
		modelItemFolderSlug,
		artifactFilename
	)
	let artifactFileContent
	try {
		artifactFileContent = await fs.readFile(artifactFilePath, 'utf8')
	} catch (error) {
		console.error(`Missing file:`, modelFilePath)
	}
	const artifact = {
		name: artifactFilename.split('.')[0],
		path: path.join('content', modelFolder, modelItemFolderSlug, artifactFilename),
		content: artifactFileContent
	}
	return artifact
}


/* public api */
const getPostures = async () => {
	return getModelFolder('postures')
}

const getPostureBySlug = async (slug) => {
	const postures = await getPostures()
	const posture = postures.find(item => {
		return item.slug === slug
	})
	return posture
}

export {
	getPostures,
	getPostureBySlug
}
