import Layout from '../components/Layout.js'
import Link from 'next/link'
import LinkExternal from '../components/LinkExternal.js'
import {getPostures} from '../utils/api.js'

function PosturesPage({postures}) {
	return (
		<Layout>
			<ul className="Postures">
				{postures && postures.map((posture, index) => (
					<li key={index}>
						<Link href={`/postures/${posture.slug}`}>
							<span className="PostureListItem">
								<span className="PostureListItem-name">
									{posture.frontmatter.name_asana}
								</span>
								<span>
									({posture.frontmatter.name_international})
								</span>
							</span>
						</Link>
					</li>
				))}
			</ul>
			<p>
				Full <LinkExternal href="https://en.wikipedia.org/wiki/List_of_asanas">list of postures (wiki)</LinkExternal>.
			</p>
		</Layout>
	)
}

export const getStaticProps = async () => {
	const postures = await getPostures()
	return {
		props: {
			postures
		},
	}
}

export default PosturesPage
