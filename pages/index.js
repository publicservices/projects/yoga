import Layout from '../components/Layout.js'
import ImaginaireA from '../components/imaginaires/imaginaire-a.js'
import Link from 'next/link'
import LinkExternal from '../components/LinkExternal.js'

function HomePage() {
	return (
		<Layout>
			<figure className="HomeImage">
				<ImaginaireA/>
				<LinkExternal
					href="https://gitlab.com/sctlib/yoga"
					className="Nav-item"
				>
					git
				</LinkExternal>
			</figure>
		</Layout>
	)
}

export default HomePage
