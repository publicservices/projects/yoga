import Layout from '../../components/Layout.js'
import Posture from '../../components/Posture.js'

import {
	getPostures,
	getPostureBySlug
} from '../../utils/api.js'
import {
	basePath
} from '../../next.config.js'

export default function PosturePage({ posture }) {
	if (!posture) return ''
	return (
		<Layout>
			{posture && <Posture posture={posture}/>}
		</Layout>
	)
}


export async function getStaticProps({params}) {
	const posture = await getPostureBySlug(params.slug)
	return {
		props: {
			posture
		},
	}
}

export async function getStaticPaths() {
	const postures = await getPostures()
	const posturesSlugs = postures.map(posture => {
		return {
			params: {
				slug: posture.slug
			}
		}
	})
	return {
		/* paths: [], */
		paths: posturesSlugs,
		fallback: false
	}
}
