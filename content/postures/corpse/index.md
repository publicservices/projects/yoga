---
name_asana: "Shavasana"
name_international: "Corpse"
name_sanskrit: "शवासन"
asana_type: "Reclining"
url_wikipedia: "https://en.wikipedia.org/wiki/Shavasana"
---

Although it looks easy, the corpse pose might be the most difficult of the asanas.

Try to scan your body mentally, while in this pose.

Pay attention to every part of your body, going from the feets to the head, and down again.
