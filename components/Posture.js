import LinkExternal from './LinkExternal.js'

const Posture = ({posture}) => {
	const {
		frontmatter,
		artifacts
	} = posture

	const {
		name_asana,
		name_international,
		name_sanskrit,
		asana_type,
		url_wikipedia
	} = frontmatter

	return (
		<article className="Posture">
			<header className="Posture-header">
				<h1
					className="Posture-name"
					title={`Asana name: ${name_asana}`}
				>
					{name_asana}
				</h1>
				<span className="Posture-asanaNames">
					(
					<span>{name_international}</span>{','}
					<span>{name_sanskrit}</span>
					)
				</span>
			</header>

			<aside className="Posture-info">
				<span
					className="Posture-type"
					title={`Asana type: ${asana_type}`}>{asana_type}</span>
				{url_wikipedia && (
					<span className="Posture-urlWikipedia">
						<LinkExternal
							href={url_wikipedia}
						>wiki</LinkExternal>
					</span>
				)}
			</aside>

			{posture.artifacts && (
				<aside className="Posture-artifacts">
					{artifacts.map((artifact, index) => (
						<figure
						className="Posture-artifact"
						key={index}
						dangerouslySetInnerHTML={{
							__html: artifact.content
						}}
						/>
					))}
				</aside>
			)}

			<div className="Posture-content">
				{posture.content}
			</div>
		</article>
	)
}

export default Posture
